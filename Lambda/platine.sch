<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="googy">
<packages>
<package name="TQFP32">
<smd name="1" x="-4.5" y="2.8" dx="1.5" dy="0.3" layer="1"/>
<smd name="2" x="-4.5" y="2" dx="1.5" dy="0.3" layer="1"/>
<smd name="3" x="-4.5" y="1.2" dx="1.5" dy="0.3" layer="1"/>
<smd name="4" x="-4.5" y="0.4" dx="1.5" dy="0.3" layer="1"/>
<smd name="5" x="-4.5" y="-0.4" dx="1.5" dy="0.3" layer="1"/>
<smd name="6" x="-4.5" y="-1.2" dx="1.5" dy="0.3" layer="1"/>
<smd name="7" x="-4.5" y="-2" dx="1.5" dy="0.3" layer="1"/>
<smd name="8" x="-4.5" y="-2.8" dx="1.5" dy="0.3" layer="1"/>
<smd name="9" x="-2.8" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="-2" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="-1.2" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="-0.4" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="0.4" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="1.2" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="2" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="2.8" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="4.5" y="-2.8" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="18" x="4.5" y="-2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="19" x="4.5" y="-1.2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="20" x="4.5" y="-0.4" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="21" x="4.5" y="0.4" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="22" x="4.5" y="1.2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="23" x="4.5" y="2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="24" x="4.5" y="2.8" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="25" x="2.8" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="26" x="2" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="27" x="1.2" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="28" x="0.4" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="29" x="-0.4" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="30" x="-1.2" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="31" x="-2" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="32" x="-2.8" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.127" layer="21"/>
<circle x="-2.71" y="2.75" radius="0.259421875" width="0.127" layer="21"/>
<text x="3.81" y="3.81" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.1" layer="39"/>
<wire x1="6.35" y1="6.35" x2="6.35" y2="-6.35" width="0.1" layer="39"/>
<wire x1="6.35" y1="-6.35" x2="-6.35" y2="-6.35" width="0.1" layer="39"/>
<wire x1="-6.35" y1="-6.35" x2="-6.35" y2="6.35" width="0.1" layer="39"/>
</package>
<package name="0805_HANDSOLDER">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.5" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<rectangle x1="-1.05" y1="-0.625" x2="-0.6" y2="0.625" layer="51"/>
<rectangle x1="0.6" y1="-0.625" x2="1.05" y2="0.625" layer="51"/>
<rectangle x1="-1" y1="0.55" x2="1" y2="0.625" layer="51"/>
<rectangle x1="-1" y1="-0.625" x2="1" y2="-0.55" layer="51"/>
</package>
<package name="1206_HANDSOLDER">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="2" x="1.7" y="0" dx="1.3" dy="1.803" layer="1"/>
<smd name="1" x="-1.7" y="0" dx="1.3" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.85" x2="-1.1" y2="0.85" layer="51"/>
<rectangle x1="1.1" y1="-0.85" x2="1.6" y2="0.85" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="-1.6" y1="-0.85" x2="1.6" y2="-0.7" layer="51"/>
<rectangle x1="-1.6" y1="0.7" x2="1.6" y2="0.85" layer="51"/>
</package>
<package name="R_SHUNT_4X2">
<smd name="1" x="0" y="1.4" dx="4" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.4" dx="4" dy="1.4" layer="1"/>
<wire x1="-1.9" y1="1.1" x2="1.9" y2="1.1" width="0.127" layer="21"/>
<wire x1="1.9" y1="1.1" x2="1.9" y2="-1.1" width="0.127" layer="21"/>
<wire x1="1.9" y1="-1.1" x2="-1.9" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.1" x2="-1.9" y2="1.1" width="0.127" layer="21"/>
<text x="2.1" y="-2" size="1.27" layer="21">&gt;VALUE</text>
<text x="2.1" y="0.8" size="1.27" layer="21">&gt;NAME</text>
</package>
<package name="R2512">
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.1" layer="21"/>
<wire x1="-3.3" y1="-1.7" x2="3.3" y2="-1.7" width="0.1" layer="21"/>
<wire x1="-3.3" y1="1.7" x2="-3.3" y2="-1.7" width="0.1" layer="21"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.1" layer="21"/>
<smd name="P$1" x="-2.9" y="0" dx="3.68" dy="1.65" layer="1" rot="R90"/>
<smd name="P$2" x="2.9" y="0" dx="3.68" dy="1.65" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="CJ125">
<wire x1="-15.24" y1="30.48" x2="15.24" y2="30.48" width="0.254" layer="94"/>
<wire x1="15.24" y1="30.48" x2="15.24" y2="-33.02" width="0.254" layer="94"/>
<wire x1="15.24" y1="-33.02" x2="-15.24" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-33.02" x2="-15.24" y2="30.48" width="0.254" layer="94"/>
<text x="-15.24" y="-35.56" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-19.05" size="1.778" layer="96">&gt;VALUE</text>
<pin name="!RST" x="20.32" y="27.94" length="middle" function="dot" rot="R180"/>
<pin name="RM" x="-20.32" y="27.94" length="middle"/>
<pin name="CM" x="-20.32" y="22.86" length="middle"/>
<pin name="RS" x="-20.32" y="17.78" length="middle"/>
<pin name="UN" x="-20.32" y="12.7" length="middle"/>
<pin name="UP" x="-20.32" y="5.08" length="middle"/>
<pin name="US" x="-20.32" y="-2.54" length="middle"/>
<pin name="VM" x="-20.32" y="-7.62" length="middle"/>
<pin name="IP" x="-20.32" y="-22.86" length="middle"/>
<pin name="IA" x="-20.32" y="-30.48" length="middle"/>
<pin name="GND" x="-2.54" y="-38.1" length="middle" direction="pwr" rot="R90"/>
<pin name="GNDS" x="2.54" y="-38.1" length="middle" direction="pwr" rot="R90"/>
<pin name="DIAHD" x="20.32" y="-30.48" length="middle" rot="R180"/>
<pin name="DIAHG" x="20.32" y="-25.4" length="middle" rot="R180"/>
<pin name="CF" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="RF" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="UA" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="UR" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="OSZ" x="20.32" y="7.62" length="middle" direction="oc" function="clk" rot="R180"/>
<pin name="SCK" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="!SS" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="SO" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="SI" x="20.32" y="22.86" length="middle" rot="R180"/>
<pin name="UB" x="5.08" y="35.56" length="middle" direction="pwr" rot="R270"/>
<pin name="VCCS" x="0" y="35.56" length="middle" direction="pwr" rot="R270"/>
<pin name="VCC" x="-5.08" y="35.56" length="middle" direction="pwr" rot="R270"/>
</symbol>
<symbol name="ATMEGA8">
<wire x1="-17.78" y1="27.94" x2="20.32" y2="27.94" width="0.254" layer="94"/>
<wire x1="20.32" y1="27.94" x2="20.32" y2="-35.56" width="0.254" layer="94"/>
<wire x1="20.32" y1="-35.56" x2="-17.78" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-35.56" x2="-17.78" y2="27.94" width="0.254" layer="94"/>
<text x="-17.78" y="-38.1" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="29.21" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PB5(SCK)" x="25.4" y="-33.02" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2)" x="-22.86" y="2.54" length="middle"/>
<pin name="PB6(XTAL1/TOSC1)" x="-22.86" y="7.62" length="middle"/>
<pin name="GND@1" x="-22.86" y="-5.08" length="middle" direction="pwr"/>
<pin name="GND@2" x="-22.86" y="-7.62" length="middle" direction="pwr"/>
<pin name="VCC@1" x="-22.86" y="-10.16" length="middle" direction="pwr"/>
<pin name="VCC@2" x="-22.86" y="-12.7" length="middle" direction="pwr"/>
<pin name="GND" x="-22.86" y="20.32" length="middle" direction="pwr"/>
<pin name="AREF" x="-22.86" y="17.78" length="middle" direction="pas"/>
<pin name="AVCC" x="-22.86" y="15.24" length="middle" direction="pwr"/>
<pin name="PB4(MISO)" x="25.4" y="-30.48" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2)" x="25.4" y="-27.94" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B)" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="PB1(OC1A)" x="25.4" y="-22.86" length="middle" rot="R180"/>
<pin name="PB0(ICP)" x="25.4" y="-20.32" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="PD6(AIN0)" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="PD5(T1)" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PD4(XCK/T0)" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PD3(INT1)" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PD2(INT0)" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PD1(TXD)" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="PD0(RXD)" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="ADC7" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="ADC6" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL)" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA)" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="PC3(ADC3)" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="PC2(ADC2)" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="PC1(ADC1)" x="25.4" y="22.86" length="middle" rot="R180"/>
<pin name="PC0(ADC0)" x="25.4" y="25.4" length="middle" rot="R180"/>
<pin name="PC6(/RESET)" x="-22.86" y="25.4" length="middle" function="dot"/>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R_EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CJ125" prefix="IC">
<description>Bosch Lambda Interface IC für LSU4.2 und LSU4.9</description>
<gates>
<gate name="G$1" symbol="CJ125" x="0" y="2.54"/>
</gates>
<devices>
<device name="LQFP32" package="TQFP32">
<connects>
<connect gate="G$1" pin="!RST" pad="7"/>
<connect gate="G$1" pin="!SS" pad="17"/>
<connect gate="G$1" pin="CF" pad="26"/>
<connect gate="G$1" pin="CM" pad="11"/>
<connect gate="G$1" pin="DIAHD" pad="6"/>
<connect gate="G$1" pin="DIAHG" pad="4"/>
<connect gate="G$1" pin="GND" pad="29"/>
<connect gate="G$1" pin="GNDS" pad="28"/>
<connect gate="G$1" pin="IA" pad="2"/>
<connect gate="G$1" pin="IP" pad="32"/>
<connect gate="G$1" pin="OSZ" pad="3"/>
<connect gate="G$1" pin="RF" pad="27"/>
<connect gate="G$1" pin="RM" pad="10"/>
<connect gate="G$1" pin="RS" pad="8"/>
<connect gate="G$1" pin="SCK" pad="13"/>
<connect gate="G$1" pin="SI" pad="15"/>
<connect gate="G$1" pin="SO" pad="14"/>
<connect gate="G$1" pin="UA" pad="23"/>
<connect gate="G$1" pin="UB" pad="30"/>
<connect gate="G$1" pin="UN" pad="31"/>
<connect gate="G$1" pin="UP" pad="22"/>
<connect gate="G$1" pin="UR" pad="12"/>
<connect gate="G$1" pin="US" pad="21"/>
<connect gate="G$1" pin="VCC" pad="19"/>
<connect gate="G$1" pin="VCCS" pad="5 18"/>
<connect gate="G$1" pin="VM" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA8" prefix="IC">
<gates>
<gate name="G$1" symbol="ATMEGA8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP32">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND" pad="21"/>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="PB0(ICP)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1A)" pad="13"/>
<connect gate="G$1" pin="PB2(SS/OC1B)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI/OC2)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1/TOSC1)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2/TOSC2)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4/SDA)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5/SCL)" pad="28"/>
<connect gate="G$1" pin="PC6(/RESET)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD)" pad="31"/>
<connect gate="G$1" pin="PD2(INT0)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1)" pad="1"/>
<connect gate="G$1" pin="PD4(XCK/T0)" pad="2"/>
<connect gate="G$1" pin="PD5(T1)" pad="9"/>
<connect gate="G$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="G$1" pin="VCC@1" pad="4"/>
<connect gate="G$1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="-2.54"/>
</gates>
<devices>
<device name="0805" package="0805_HANDSOLDER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206_HANDSOLDER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R_EU" x="0" y="0"/>
</gates>
<devices>
<device name="R1206" package="1206_HANDSOLDER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHUNT_4MMX2MM" package="R_SHUNT_4X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="0805_HANDSOLDER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512_SHUNT" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+12V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+12V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+12V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="googy" deviceset="CJ125" device="LQFP32"/>
<part name="IC2" library="googy" deviceset="ATMEGA8" device=""/>
<part name="C1" library="googy" deviceset="C" device="0805"/>
<part name="R1" library="googy" deviceset="R" device="R0805"/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" deviceset="+12V" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="R2" library="googy" deviceset="R" device="R0805"/>
<part name="R3" library="googy" deviceset="R" device="R0805"/>
<part name="R4" library="googy" deviceset="R" device="R0805"/>
<part name="R5" library="googy" deviceset="R" device="R0805"/>
<part name="R6" library="googy" deviceset="R" device="R0805"/>
<part name="R7" library="googy" deviceset="R" device="R0805"/>
<part name="C2" library="googy" deviceset="C" device="0805"/>
<part name="R8" library="googy" deviceset="R" device="R0805"/>
<part name="C3" library="googy" deviceset="C" device="0805"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="C4" library="googy" deviceset="C" device="0805"/>
<part name="C5" library="googy" deviceset="C" device="0805"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="R9" library="googy" deviceset="R" device="R0805"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="R10" library="googy" deviceset="R" device="R0805"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="C6" library="googy" deviceset="C" device="0805"/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="C7" library="googy" deviceset="C" device="0805"/>
<part name="C8" library="googy" deviceset="C" device="0805"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="0" y="0"/>
<instance part="IC2" gate="G$1" x="104.14" y="2.54"/>
<instance part="C1" gate="G$1" x="-76.2" y="25.4"/>
<instance part="R1" gate="G$1" x="-76.2" y="10.16"/>
<instance part="P+1" gate="1" x="-5.08" y="55.88"/>
<instance part="P+2" gate="1" x="5.08" y="55.88"/>
<instance part="GND1" gate="1" x="-2.54" y="-48.26"/>
<instance part="P+3" gate="1" x="76.2" y="-5.08"/>
<instance part="R2" gate="G$1" x="-30.48" y="17.78"/>
<instance part="R3" gate="G$1" x="-30.48" y="-2.54"/>
<instance part="R4" gate="G$1" x="-25.4" y="27.94"/>
<instance part="R5" gate="G$1" x="-45.72" y="10.16" rot="R90"/>
<instance part="R6" gate="G$1" x="-45.72" y="-10.16" rot="R90"/>
<instance part="R7" gate="G$1" x="-35.56" y="-25.4" rot="R90"/>
<instance part="C2" gate="G$1" x="-27.94" y="22.86" rot="R90"/>
<instance part="R8" gate="G$1" x="33.02" y="-10.16"/>
<instance part="C3" gate="G$1" x="43.18" y="-15.24"/>
<instance part="GND2" gate="1" x="43.18" y="-20.32"/>
<instance part="C4" gate="G$1" x="55.88" y="-10.16"/>
<instance part="C5" gate="G$1" x="60.96" y="-10.16"/>
<instance part="GND3" gate="1" x="55.88" y="-15.24"/>
<instance part="GND4" gate="1" x="60.96" y="-15.24"/>
<instance part="R9" gate="G$1" x="27.94" y="7.62"/>
<instance part="GND5" gate="1" x="38.1" y="7.62" rot="R90"/>
<instance part="R10" gate="G$1" x="30.48" y="63.5" rot="R90"/>
<instance part="GND6" gate="1" x="30.48" y="48.26"/>
<instance part="C6" gate="G$1" x="30.48" y="53.34"/>
<instance part="P+4" gate="1" x="30.48" y="71.12"/>
<instance part="C7" gate="G$1" x="-12.7" y="43.18"/>
<instance part="C8" gate="G$1" x="12.7" y="48.26"/>
<instance part="GND7" gate="1" x="-12.7" y="38.1"/>
<instance part="GND8" gate="1" x="12.7" y="43.18"/>
<instance part="GND9" gate="1" x="78.74" y="-15.24"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="AVCC"/>
<wire x1="81.28" y1="17.78" x2="50.8" y2="17.78" width="0.1524" layer="91"/>
<wire x1="50.8" y1="17.78" x2="50.8" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VCCS"/>
<wire x1="50.8" y1="35.56" x2="0" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GNDS"/>
<wire x1="2.54" y1="-38.1" x2="71.12" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-38.1" x2="71.12" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="UB"/>
<pinref part="P+2" gate="1" pin="+12V"/>
<wire x1="5.08" y1="35.56" x2="5.08" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="12.7" y1="53.34" x2="5.08" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="-5.08" y1="35.56" x2="-5.08" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="48.26" x2="-5.08" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="48.26" x2="-5.08" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VCC@2"/>
<wire x1="81.28" y1="-10.16" x2="76.2" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-10.16" x2="76.2" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VCC@1"/>
<wire x1="76.2" y1="-7.62" x2="81.28" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<junction x="76.2" y="-7.62"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-2.54" y1="-38.1" x2="-2.54" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="33.02" y1="7.62" x2="35.56" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND@1"/>
<wire x1="81.28" y1="-2.54" x2="78.74" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="78.74" y1="-2.54" x2="78.74" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GND@2"/>
<wire x1="78.74" y1="-5.08" x2="78.74" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-5.08" x2="78.74" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="IA"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-30.48" x2="-35.56" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="IP"/>
<wire x1="-20.32" y1="-22.86" x2="-30.48" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-22.86" x2="-30.48" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="-20.32" x2="-35.56" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="-20.32" x2="-45.72" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="-20.32" x2="-45.72" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="UN"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-20.32" y1="12.7" x2="-43.18" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="12.7" x2="-45.72" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="US"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-20.32" y1="-2.54" x2="-25.4" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="UP"/>
<wire x1="-20.32" y1="5.08" x2="-45.72" y2="5.08" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="-2.54" x2="-45.72" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="-5.08" x2="-45.72" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="-2.54" x2="-45.72" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RS"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-20.32" y1="17.78" x2="-25.4" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VM"/>
<wire x1="-20.32" y1="-7.62" x2="-50.8" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="-7.62" x2="-50.8" y2="17.78" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="17.78" x2="-35.56" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CM"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-20.32" y1="22.86" x2="-25.4" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-33.02" y1="22.86" x2="-38.1" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="22.86" x2="-38.1" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="27.94" x2="-30.48" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="IC1" gate="G$1" pin="RM"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RF"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="20.32" y1="-10.16" x2="27.94" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="38.1" y1="-10.16" x2="43.18" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="CF"/>
<wire x1="20.32" y1="-17.78" x2="38.1" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-17.78" x2="38.1" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="UR"/>
<wire x1="20.32" y1="2.54" x2="60.96" y2="2.54" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="60.96" y1="2.54" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="PC0(ADC0)"/>
<wire x1="129.54" y1="27.94" x2="129.54" y2="33.02" width="0.1524" layer="91"/>
<wire x1="129.54" y1="33.02" x2="68.58" y2="33.02" width="0.1524" layer="91"/>
<wire x1="68.58" y1="33.02" x2="68.58" y2="2.54" width="0.1524" layer="91"/>
<wire x1="68.58" y1="2.54" x2="60.96" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="UA"/>
<wire x1="55.88" y1="-5.08" x2="20.32" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="PC1(ADC1)"/>
<wire x1="129.54" y1="25.4" x2="132.08" y2="25.4" width="0.1524" layer="91"/>
<wire x1="132.08" y1="25.4" x2="132.08" y2="35.56" width="0.1524" layer="91"/>
<wire x1="132.08" y1="35.56" x2="73.66" y2="35.56" width="0.1524" layer="91"/>
<wire x1="73.66" y1="35.56" x2="73.66" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-5.08" x2="55.88" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="OSZ"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="20.32" y1="7.62" x2="22.86" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="!RST"/>
<wire x1="20.32" y1="27.94" x2="20.32" y2="58.42" width="0.1524" layer="91"/>
<wire x1="20.32" y1="58.42" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<junction x="30.48" y="58.42"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SI"/>
<wire x1="20.32" y1="22.86" x2="66.04" y2="22.86" width="0.1524" layer="91"/>
<wire x1="66.04" y1="22.86" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="38.1" x2="149.86" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="PB3(MOSI/OC2)"/>
<wire x1="149.86" y1="38.1" x2="149.86" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-25.4" x2="129.54" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SO"/>
<wire x1="20.32" y1="20.32" x2="63.5" y2="20.32" width="0.1524" layer="91"/>
<wire x1="63.5" y1="20.32" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="63.5" y1="40.64" x2="152.4" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="PB4(MISO)"/>
<wire x1="152.4" y1="40.64" x2="152.4" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="152.4" y1="-27.94" x2="129.54" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="PB5(SCK)"/>
<wire x1="129.54" y1="-30.48" x2="154.94" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-30.48" x2="154.94" y2="43.18" width="0.1524" layer="91"/>
<wire x1="154.94" y1="43.18" x2="60.96" y2="43.18" width="0.1524" layer="91"/>
<wire x1="60.96" y1="43.18" x2="60.96" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="SCK"/>
<wire x1="60.96" y1="15.24" x2="20.32" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="!SS"/>
<wire x1="20.32" y1="17.78" x2="45.72" y2="17.78" width="0.1524" layer="91"/>
<wire x1="45.72" y1="17.78" x2="45.72" y2="12.7" width="0.1524" layer="91"/>
<wire x1="45.72" y1="12.7" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="12.7" x2="58.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="58.42" y1="45.72" x2="157.48" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="PB2(SS/OC1B)"/>
<wire x1="157.48" y1="45.72" x2="157.48" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-22.86" x2="129.54" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
